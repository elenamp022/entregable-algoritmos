use lib('.');
use variaciones;

## MÓDULO

# Parte 1.1. Subrutina para fusionar dos cadenas
sub fusionar {my($c1,$c2) = @_;
	
	my %solucion;	# Se define el hash que va a contener la solución
		
	$solucion {"cadena1"} = $c1;	# El valor de la cadena 1 se puede introducir directamente
	$solucion {"cadena2"} = $c2;	# El valor de la cadena 2 también se puede introducir directamente

	my $menor = 0;
	my @solapamientos;
	my $mayor_solapamiento = 0;

	if (length $c1 > length$c2) {$menor = $c2} else {$menor = $c1} ;	# Se evalúa cual es la cadena más pequeña porque esa va a determinar el número de iteraciones del bucle

	foreach my $n (1..length ($menor)) {
		if (((substr $c1, -$n) eq (substr $c2, 0, $n)) # Se comprueba que ambos extremos sean iguales (el extremo derecho de la cadena 1 y el izquierdo de la cadena 2)
		and ($n > $mayor_solapamiento)) {	# Se comprueba si el número de solapamiento es el mayor al que se ha llegado
			$mayor_solapamiento = $n	# Si lo es se guarda como la variable mayor_solapamiento
			}		
	}

	$solucion{"solapamiento"} = $mayor_solapamiento;	# mayor_solapamiento corresponde al mayor número de nucleótidos que solopan en ese orden
	$solucion{"fusion"} = $c1.substr $c2, $mayor_solapamiento, ;	# Se forma la cadena fusión

	return %solucion;
}1;

# Parte 1.2. Subrutina para fusionar una lista de cadenas en el orden por defecto
sub fusionar_lista {my(@lista) = @_;

	my $lista_fusionada;	# Se define la variable que va a contener la solución

	foreach my $pos (1..$#lista) {	# Se itera sobre las posiciones de la lista, saltandose la posición 0
		if ($pos == 1) {	
			my %dic = fusionar($lista[0], $lista[1]);	# En el caso de la posición igual a 1 se fusionan los dos primeros elementos de la lista y se recupera, del diccionario creado por la funcion fusionar(), la cadena fusionada
			$lista_fusionada = $dic{"fusion"}}
		else {
			my %dic2 = fusionar($lista_fusionada, $lista[$pos]); # En el resto de iteraciones se utiliza la cadena fusionada para fusionarla con el elemento de la lista por cuya posición está iterando
			$lista_fusionada = $dic2{"fusion"}}
	}
	return $lista_fusionada
}1;

# Parte 2. Subrutina fusionar las cadenas de una lista de forma que se forma la cadena más pequña posible. Búsqueda exhaustiva
sub menor_cadena_BE {my(@seq) = @_;
	
	my @actual = @seq;

	my $menor_num = 200;
	my $menor_seq = "";
	while (@actual) { # Mientras queden combinaciones
		my $conjunto = fusionar_lista (@actual);	# Se utiliza la función del módulo fusionar para unir las cadenas
		my $longitud = length $conjunto; 	# Calcular la longitud de la cadena fusionada
		
		if ($longitud < $menor_num) {
			$menor_num = $longitud;
			$menor_seq = $conjunto;
			
			}
		# print $longitud, "\n";
		
		@actual = permutar @seq, @actual; # Siguiente combinación
	}

	return $menor_seq;
}1;

# Parte 3. Subrutina fusionar las cadenas de una lista de forma que se forma la cadena más pequña posible. Avance rápido

sub menor_cadena_AR {my(@seq) = @_;

	my @numeros = 0..$#seq;
	my $longitud = 2;

	my @actual = primera_variacion(@numeros, $longitud); 	# Subrutina que devuelve todas las posibles combinaciones de cadenas de dos en dos
	my @lista_hash_fusiones;

	while (@actual) {	# Mientras queden combinaciones
		
		if ($actual[0] != $actual[1]) {		# Si las cadenas no son iguales
			
			my %cadena_fusionada = fusionar ($seq[$actual[0]], $seq[$actual[1]]); # Obtener la fusión de ellas
			push @lista_hash_fusiones, \%cadena_fusionada};	# Guardar el hash obtenido en una lista de hashes
			
		@actual = siguiente_variacion(@numeros, @actual)	# Pasar a la siguente combinación
	}


	my @lista_ordenada = sort {$$b{"solapamiento"} <=> $$a{"solapamiento"}} @lista_hash_fusiones;	#Ordenar la lista de hashes según el solapamiento tras la fusión de cadenas

	my %candidatos;
	foreach my $pos (0..$#seq) {	# Se crea un diccionarion que contiente como claves las cadenas originales
		$candidatos{$seq[$pos]} = 1;
		}


	my @solucion;

	while (@lista_ordenada) { # Mientras queden hashes en la lista ordenada de hashes
		my $c1 = $lista_ordenada[0]{"cadena1"};		# Se obtienen las dos cadenas de las que procede la fusión
		my $c2 = $lista_ordenada[0]{"cadena2"};
		
		if (exists $candidatos{$c1} and exists $candidatos{$c2}) {	# Si ambas de estas cadenas están dentro del hash candidatos
			push @solucion, $c1;	# Incluir ambas cadenas en la lista @solución, que contiene las cadenas originales en el orden más óptimo
			push @solucion, $c2;	
			
			delete $candidatos{$c1};	# Eliminar ambas cadenas para evitar que vuelvan a entrar de nuevo
			delete $candidatos{$c2};
			}
		
		shift @lista_ordenada;	# Eliminar el primer elemento de la lista de hashes ordenados ya que ya ha sido evaluado
		}



	my $fusion_final;

	if (!(!keys %candidatos)) {	# En el caso de que el número de cadenas originales sea impar, va a quedar una sulta en el hash candidatos. Si el hash no está vacío
		my $cadena_sobra = (keys %candidatos)[0];	# Obtener la cadena que sobra
		
		my $fusion_provisional = fusionar_lista (@solucion);	# Obtener la solución que obtendríamos si se tuviera en cuenta la cadena que sobra
		
		my %sol_principio = fusionar ($cadena_sobra, $fusion_provisional); 	# Calcular la fusión de la cadena que sobra con la solución provisonal, por el principio y por el final
		my %sol_final = fusionar ($fusion_provisional, $cadena_sobra);
		
		if ($sol_principio{"solapamiento"} > $sol_final{"solapamiento"}){	# Se evalua qúe solapamiento es el mayor y ese será el orden definitivo
			$fusion_final = $sol_principio{"fusion"}}
			
		else {
			$fusion_final = $sol_final{"fusion"}}
			
		}
	else {	# Si el número de cadenas originales no era impar, no quedará ninguna cadena en el hash candidatos
		$fusion_final = fusionar_lista (@solucion);}


	return $fusion_final;

}1;
