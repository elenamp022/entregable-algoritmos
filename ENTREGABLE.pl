use strict;
use warnings;

use lib('.');
use MODULO;

## LLAMADAS A LAS FUNCIONES

# Parte 1.1. Fusionar dos cadenas de texto

my $c1 = "AACCTG";
my $c2 = "TGAAACTCA";

my %fusionado = fusionar($c1, $c2);

print "Al fusionar las cadenas ", $c1, " y ", $c2, " en este orden se obtiene el siguiente resultado:", "\n";


foreach my $key (keys %fusionado) {
		print $key, " => ", $fusionado{$key}, "\n"
	}

print "\n";


## Parte 1.2. Fusionar las cadenas de texto en el orden por defecto

my @lista = ("AACCTG", "TGAAACTCA", "ACCA");

my $lista_fusionada = fusionar_lista(@lista);

print "Al fusionar las cadenas ", join (",", @lista), " en este orden se obtiene la siguiente cadena:", "\n";

print $lista_fusionada, "\n\n";


# Parte 2. Encontrar la cadena más pequeña que se forma al combinar las distintas cadenas en diferente orden. Busqueda exhaustiva

my @secuencias = ("CTGACAA", "CCGCAATC", "CTAAGT", "AAAGCTA", "TCTCCCA");

my $menor_secuencia = menor_cadena_BE (@secuencias);

print "La cadena más pequeña que se obtiene al fusionar ", join (",", @secuencias), " es la siguiente: (BUSQUEDA EXHAUSTIVA)", "\n";

print $menor_secuencia, "\n";

print "La logitud de esta cadena es: ", length $menor_secuencia, "\n\n";


# Parte 3. Encontrar la cadena más pequeña que se forma al combinar las distintas cadenas en diferente orden. Avance rápido

my $resultado = menor_cadena_AR (@secuencias);

print "La cadena más pequeña que se obtiene al fusionar ", join (",", @secuencias), " es la siguiente: (AVANCE RÁPIDO)", "\n";

print $resultado, "\n";

print "La longitud de esta cadena es: ", length $menor_secuencia, "\n";
